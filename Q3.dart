class App {
  String name;
  String category;
  String developer;
  int year;

  App(this.name, this.category, this.developer, this.year);

  void describe() {
    print("App name: $name");
    print("Category: $category");
    print("Developer: $developer");
    print("Year won: $year");
  }

  String toUpper() {
    return this.name.toUpperCase();
  }
}

void main() {
  var lastWinner = App("Ambani Africa", "Educational", "Mukundi Lambani", 2021);

  lastWinner.describe();
  print(lastWinner.toUpper());
}
